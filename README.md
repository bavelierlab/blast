<p align="center">
  <a href=""><img src="./assets/img/blast.png" alt="blast task"></a>
</p>

# Table of contents

- [Description](#-description)
- [Online demo](#-online-demo)
- [Usage](#%EF%B8%8F-how-to-use-it)
- [Data dictionary](#-data-dictionary)
- [What could differ from Lachaux (2018)](#%EF%B8%8F-what-could-differ-from-lachaux-2018)
- [Citation](#-citation)
- [References](#-references)

---

# 📝 Description

When using this task, you can copy-paste this in your Methods' section of your manuscript :

> (Version 1.0, October 2024) This task has been adapted by the Bavelierlab (Rioja & Denkinger, 2024) based on a ... task as implement in ... . It is a ... task where participants are instructed to respond with a given key when a previously shown letter is part of an array of four letters and to respond with another key when the letter is not part of the array. To familiarize with the task, a practice session of four trials (NoGo, Go, NoGo, Go) is first presented. The rule to pass the practice session is to have at least 3 correct trials and a maximum of 3 practice blocks is allowed. Feedback is given after each practice trial with the 5-hash mask becoming green (correct response) or red (incorrect response). The four practice trials are displaying the same target letter and the same array letters in the same order (e.g., 'D' followed by 'FABU', 'W' followed by 'WQZN', 'T' followed by 'KXGV', 'C' followed by 'FRCH'). The task is then composed of one single block of 120 trials. Each target letter is randomly chosen among this array of possible letters (called letter_array) ["B","D", "F", "G", "H", "K", "L", "N", "P", "T", "V", "Z"] and the following array, of three other letters for Go trials or four other letters for NoGo trials, is composed of letters from letter_array. The code makes sure to use as target all letters from letter_array once before repeating the same letter (e.g., there is no possible letter repetition before n + letter_array.length - 1). Moreover, the position of the target letter cannot be the same between n and n+1 trials. The number of Go and NoGo trials is not set, in general the code give an average of 50% Go and 50% NoGo.
Each target letter is flashed in the middle of the screen during 200 ms (presentation time), a 1-hash mask is displayed during 500 ms and a 4-letter array with a hash at the center is displayed during 3800 ms maximum. After response a 5-hash stimulus is displayed during 300 ms followed by a 1-hash mask of 800 ms buffer time if correct response or 3800 ms if incorrect response. Letter width is set at 0.5 visual angle (VA) and letter distance from the center of the screen is set at 0.707 VA giving a total width of the array at 1.5 VA. RT and trial accuracy (hit, FA, mis, CR) are saved throughout the task.

# Parameters

See ./src/blast-params.json ([here](/src/blast-params.json)), for version 1.0

See below the explanation for each parameter:

```json
{
    "pres_time_mask5_intro" // int, presentation time (ms) for the first mask with 5 #
    "pres_time_mask1_intro" // int, presentation time (ms) for the first mask with 1 #

    "pres_time_target" // int, presentation time (ms) for the target
    "pres_time_mask1" // int, presentation time (ms) for the masks with 1 #
    "pres_time_mask5" // int, presentation time (ms) for the masks with 5 #

    "pres_time_array" // int, presentation time (ms) of the array
    "buffer_time_correct" // int, buffer time (ms) after a correct answer
    "buffer_time_incorrect" // int, buffer time (ms) after an incorrect answer, value with penalization, initially 4800ms in Lachaux

    "response_key" // {"present":char,"absent":char}, keys to press for answering present and absent

    "letter_width_va" // double, letter width in visual angle
    "letter_dist_from_center_va" // double, letter distance from center in visual angle

    "letter_color" // {"r":int, "g":int, "b":int}, letter color in RGB
    "background_color" // {"r":int, "g":int, "b":int}, background color in RGB

    "target_practice" // array of 1 char, targets to present during practice in the order of trials
    "array_practice" // array of 4 chars (the first being top left, the second top right, the third bottom left, the fourth bottom right), 4-letters arrays to present during practice in the order of trials
    "repeat_practice_times" // int, maximum number of practice repetition if not more than half of correct trials
    "letter_array" // array of 1 char, letters to be shown during the whole experiment, function 'generateArrays' takes this letter array to create an array of n_trials of 4-char
    "n_trials" // number of trials for the main task

    "testmode" // bool, when true, shows the question at the beginning of the experiment to ask 1 no test (task with n_trials trials) or 0 test (10 trials)
    "n_test_trials" // int, number of trials for testmode
    "testonly" // bool, false by default, updated to true when participant answers 0 to testmode
    "questionnaire_end" // bool, if true, asks questionnaire at the end of the task
    "savedb" // bool, when true, saves to db, see db_config.php
    "redirect" // bool, when true, redirects to redirect_address at the end of the task
    "redirect_address" // string, path or URL of redirection at the end of experiment, works only when redirect == true
}
```

# 📈 Online demo

Click [here](https://tecfaetu.unige.ch/etu-maltt/baldur/rioja0/bl/blast/) to test the task itself. At the end of the task, a popup window will ask you to download the data.

# Usage

## URL parameters

`subject_id`, `study_id`, `session_id`, `monitor_size_inch`, if you put them in URL, e.g., mywebsite.org/?subject_id=johndoe, you will automatically get subject_id and skip the question about it

## Local

- Clone this repo or download the zip (+ unzip it).

- Use an [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment) ! I personally use [Visual Studio Code (VSC)](https://code.visualstudio.com/).

- Once you have downloaded it, you can open the cloned/downloaded repo/folder in VSC,

- Open the index.html file on VSC.

- Make sure you have *Live Server* VSC Extension installed, you must have a "Go Live" on the bottom right of the screen (Keyboard shortcut should be Cmd+L followed by Cmd+O) – if you don't download the extension.

- "Go Live" and you are ready to test the task!

- To change task parameters, only change what you want in `./src/blast-params.json`

## Production 

For exporting this into your server:

1. In src/blast-params.json, change savedb to true, change redirect to true, change redirect_address and redirect_text

2. At the root of the repo, `cp .envcopy .env`, and change values to the one corresponding to your db

3. rsync the repo to your server: `rsync -avPr --exclude .git -e 'ssh -i /path/to/pem' /path/to/your/file username@host:/path/to/your/project/on/ec2`

4. In your MySQL database, copy-paste the sql query found under db/db-structure.sql, you must now have a tova table ready to be populated

5. Go to your task online and it should now work!

# 📖 Data dictionary

Click [here](/assets/refs/datadictionnary_blast.txt) to view the description of the collected variables. A must-read before analyzing data!

# ⚡️ What could differ from Lachaux (2018)

- All presentation times were copied from the Lachaux versionn. In our version 1.0, we have: 1-hash mask in intro = 800ms, presentation time (PT), except the 5 sec of 5-hashes mask which has been reduced to 3 sec
- We do not penalize (+800ms after response for correct, +4800ms for incorrect) to make the task more challenging and obtaining a better variation detection in an adult population.

# 🛟 Citation

## APA

> Rioja, K., & Denkinger, S. (2024). Bavelierlab adapted BLAST from Lachaux (2018) (Version 1.0) [Computer software].

# 🔭 References

de Leeuw, J.R. (2015). jsPsych: A JavaScript library for creating behavioral experiments in a Web browser. *Behavior Research Methods, 47*(1), 1-12. [doi:10.3758/s13428-014-0458-y](https://doi.org/10.3758/s13428-014-0458-y)

Li, Q., Joo, S. J., Yeatman, J. D., & Reinecke, K. (2020). Controlling for Participants’ Viewing Distance in Large-Scale, Psychophysical Online Experiments Using a Virtual Chinrest. *Scientific Reports, 10*(1), 1-11. doi: [10.1038/s41598-019-57204-1](https://doi.org10.1038/s41598-019-57204-1)
