/**
 * @title BLAST
 * @description The task is an adapted version of BLAST from Lachaux (2019). A short computerized test to measure the ability to stay on task.
 * @version 1.1.2
 *
 * @assets assets/
 */

// Import: functions and task parameters
import task_params from './blast-params.json' with {type: "json"};

$(document).ready(function () {
  $(document).on('keydown', function (e) {
    e = e || window.event;
    if (e.ctrlKey || e.metaKey) {
      let c = e.which || e.keyCode;
      if (c == 82) {
        e.preventDefault();
        e.stopPropagation();
      }
    }
  });
});

// prevent getting back or closing the window : https://stackoverflow.com/questions/12381563/how-can-i-stop-the-browser-back-button-using-javascript
window.onbeforeunload = function () { return `Your work will be lost.`; };

var blast = async() => {

  // ##########################
  // ### initialize jsPsych ###
  // ##########################
  var jsPsych = initJsPsych();

  // Task parameters 
  const blast_params = task_params;
  var repeat_practice = [];
  const present_practice = targetPresent(blast_params.target_practice, blast_params.array_practice);

  // Timelines
  var welcome = [];
  var practice = [];
  var main = [];

  // ***************
  // *** WELCOME ***
  // ***************

  // Preload assets 
  welcome.push({
    type: jsPsychPreload,
    images: [
      blast_params.root_path + 'assets/img/blast.png',
      blast_params.root_path + 'assets/img/card.png',
      blast_params.root_path + 'assets/img/d.png',
      blast_params.root_path + 'assets/img/ltdz.png',
      blast_params.root_path + 'assets/img/ltdzG.png',
      blast_params.root_path + 'assets/img/pltbR.png'
    ],
    data: {
      trial_name: "preload",
      block: "welcome"
    },
    on_finish: function () {
      jsPsych.data.addProperties({
        log_start: logCurrentDateTime(),
      });
    }
  });

  // Switch to fullscreen
  welcome.push({
    type: jsPsychFullscreen,
    message: "<p>The experiment will switch to full screen mode when you press the button below</p>" +
      "<p>(Version 1.1, December 2024)</p>",
    data: {
      trial_name: "fullscreen",
      block: "welcome"
    },
    fullscreen_mode: true,
  });

  // Browser check
  welcome.push({
    type: jsPsychBrowserCheck,
    skip_features: ['width', 'height', 'webaudio', 'webcam', 'microphone'],
    data: {
      trial_name: "browser_check",
      block: "welcome"
    }
  })

  // Questionnaires about id, monitorsize and testmode
  // Note that 'id' and 'monitorsize' can be URL parameters, if your URL is like this 'www.mysite.org/tova?id=001&monitorsize=13', it will automatically skip the id and monitorsize questions and save it

  // input variables to get from url
  const get_subject_id = jsPsych.data.getURLVariable('subject_id');
  const get_study_id = jsPsych.data.getURLVariable('study_id');
  const get_session_id = jsPsych.data.getURLVariable('session_id');
  const get_monitor_size_inch = jsPsych.data.getURLVariable('monitor_size_inch');

  let survey_elements_begin = [];
  if (!get_subject_id)
    survey_elements_begin.push({
      type: 'text',
      title: 'Subject ID:',
      name: 'pp_id',
      defaultValue: get_subject_id ? get_subject_id : null,
      isRequired: true,
      size: 30,
    });
  if (!get_study_id)
    survey_elements_begin.push({
      type: 'text',
      title: 'Study ID:',
      name: 'study_id',
      defaultValue: get_study_id ? get_study_id : null,
      isRequired: true,
      size: 30,
    });
  if (!get_session_id)
    survey_elements_begin.push({
      type: 'text',
      title: 'Session ID:',
      name: 'session_id',
      defaultValue: get_session_id ? get_session_id : null,
      isRequired: true,
      size: 100,
    });
  if (!get_monitor_size_inch) // or in cookies, but need to implement here
    survey_elements_begin.push({
      type: 'text',
      title: 'What is your screen size in inch?',
      name: 'monitorsize_inch',
      defaultValue: get_monitor_size_inch ? get_monitor_size_inch : null,
      isRequired: true,
      inputType: 'number',
      min: 0,
      max: 50,
    });
  if (blast_params.testmode)
    survey_elements_begin.push({
      type: 'text',
      title: '[TEST-MODE] Enter 1 for the complete experiment (120 trials) or 0 for testing the experiment (' + blast_params.n_test_trials + ' trials)',
      name: 'testonly',
      defaultValue: 0,
      isRequired: true,
      inputType: 'number',
      min: 0,
      max: 1,
    });
  // Show questionnaire if there are questions to ask (id, monitorsize or testmode, see survey_elements_begin)
  if (survey_elements_begin.length != 0) {
    welcome.push({
      type: jsPsychSurvey,
      data: {
        trial_name: "survey",
        block: "welcome"
      },
      survey_json: {
        showQuestionNumbers: false,
        pages: [
          {
            name: 'page1',
            elements: survey_elements_begin
          }
        ],
      },
      on_finish: function (data) {
        // add variables to data
        jsPsych.data.addProperties({
          subject_id: get_subject_id ? get_subject_id : data.response.pp_id,
          study_id: get_study_id ? get_study_id : data.response.study_id,
          session_id: get_session_id ? get_session_id : data.response.session_id,
          monitor_size_inch: get_monitor_size_inch ? get_monitor_size_inch : data.response.monitorsize_inch // this variable is unused in the code, it is not mandatory to compute the real size on screen since virtual chinrest is using the viewing distance
        });
        blast_params.testonly = data.response.testonly == '0';
        data.response = '';
      }
    });
  }

  // Virtual chinrest
  welcome.push({
    type: jsPsychVirtualChinrest,
    data: {
      trial_name: "virtual_chinrest",
      block: "welcome"
    },
    blindspot_reps: 2,
    resize_units: "none",
    item_path: "assets/img/card.png",
    // Add data on all trials
    on_finish: function () {
      const px2deg = jsPsych.data.get().last(1).values()[0].px2deg;
      const view_dist_cm = jsPsych.data.get().last(1).values()[0].view_dist_mm * 0.1;
      jsPsych.data.addProperties({
        _px2deg: round3deci(px2deg),
        _view_dist_cm: round3deci(view_dist_cm),
        _letter_width_va: round3deci(blast_params.letter_width_va),
        _letter_width_cm: round3deci(vaToCm(blast_params.letter_width_va, view_dist_cm)),
        _letter_dist_from_center_va: round3deci(blast_params.letter_dist_from_center_va),
        _letter_dist_from_center_cm: round3deci(vaToCm(blast_params.letter_dist_from_center_va, view_dist_cm)),
        _monitor_width_px: window.outerWidth,
        _monitor_height_px: window.outerHeight
      });
      document.getElementsByClassName("jspsych-content-wrapper")[0].remove();
    }
  });

  // Run welcome
  await jsPsych.run(welcome);

  // ****************  
  // *** PRACTICE ***
  // ****************  

  // Practice Instructions
  practice.push({
    type: jsPsychInstructions,
    data: {
      trial_name: "instructions",
      block: "practice"
    },
    pages: [
      `Welcome to our task.`,
      `This task is easy. You will first see a letter on the screen (target) <img src="assets/img/d.png" style="width:` + getLetterDistFromCenterPx(jsPsych, blast_params) * 2 + `px; vertical-align: middle;"></img><br><br>` +
      `Followed by 4 letters <img src="assets/img/ltdz.png" style="width:` + getLetterDistFromCenterPx(jsPsych, blast_params) * 2 + `px; vertical-align: middle;"></img><br><br>` +
      `If the target letter is` + ` <span style="color:MediumSeaGreen;">present</span>` + `, press the ` + `<span style="color:MediumSeaGreen;"><b style="font-size:xx-large;">F</b>` + ` key with your left finger</span> ` + `<img src="assets/img/ltdzG.png" style="width:` + getLetterDistFromCenterPx(jsPsych, blast_params) * 2 + `px; vertical-align: middle"></img><br><br>` +
      `If the target letter is ` + `<span style="color:Tomato;">NOT present</span>` + `, press the ` + `<span style="color:Tomato;"><b style="font-size:xx-large;">J</b>` + ` key with your right finger</span>` + ` <img src="assets/img/pltbR.png" style="width:` + getLetterDistFromCenterPx(jsPsych, blast_params) * 2 + `px; vertical-align: middle"></img><br><br>`,
      `The goal is to be as FAST AND ACCURATE as you can.<br> It is like driving a car, you want to drive as fast as you can, AND avoid making an accident.<br>` +
      `If you do not give any response, the next trial will automatically start after a short period<br>`,
      `Let's start with some practice. Remember, the goal is to be as fast as you can AND make as less errors as you can.`,
    ],
    show_clickable_nav: true,
    on_finish: function () {
      // Hide cursor
      document.body.style.cursor = "none";
    }
  })

  // Getting Ready: 5 sec mask5 followed by .8 sec mask1
  const waitmask5 = {
    type: jsPsychHtmlKeyboardResponse,
    stimulus: getMask5(jsPsych, blast_params, 0),
    choices: `NO_KEYS`,
    trial_duration: blast_params.pres_time_mask5_intro,
    data: {
      trial_duration: blast_params.pres_time_mask5_intro,
      trial_name: "mask5_intro",
      real_trial_index: '',
      block: "practice"
    },
    on_finish: function (data) {
      data.real_trial_index = jsPsych.data.get().last(2).values()[0].real_trial_index;
    },
  };

  const waitmask1 = {
    type: jsPsychHtmlKeyboardResponse,
    stimulus: getMask1(jsPsych, blast_params),
    choices: `NO_KEYS`,
    trial_duration: blast_params.pres_time_mask1_intro,
    data: {
      trial_duration: blast_params.pres_time_mask1_intro,
      trial_name: "mask1_intro",
      real_trial_index: '',
      block: "practice"
    },
    on_finish: function (data) {
      data.real_trial_index = jsPsych.data.get().last(2).values()[0].real_trial_index;
    },
  };

  // Timeline variables and timeline
  const block_practice = {
    timeline_variables: createBlastTimelineVariables(
      blast_params.target_practice,
      blast_params.array_practice,
      present_practice,
      jsPsych,
      blast_params
    ),
    timeline: createBlastTimeline("practice", jsPsych, blast_params),
    on_timeline_finish: function () {
      // Show cursor
      document.body.style.cursor = "auto";
    }
  };

  const repeat_prac_message = { // https://www.youtube.com/watch?v=LP7o0iAALik
    type: jsPsychHtmlKeyboardResponse,
    data: {
      trial_name: "instructions",
      block: "practice",
      real_trial_index: '',
    },
    choices: [' '],
    stimulus: `
                <p>You will go through another practice block. Please be as accurate as possible.</p>
                <p>If the `+ ` <span style="color:MediumSeaGreen;">target letter is present</span>` + `, press the ` + `<span style="color:MediumSeaGreen;"><b>F</b>` + ` key with your left finger</span></p>
                <p>If the `+ `<span style="color:Tomato;">target letter is NOT present</span>` + `, press the ` + `<span style="color:Tomato;"><b>J</b>` + ` key with your right finger</span></p>
                <p>Press the spacebar to continue.<p>
                `,
    on_start: function () {
      document.body.style.cursor = 'auto'; // display cursor during instructions
    },
    on_finish: function (data) {
      document.body.style.cursor = 'none'; // hide cursor during task
      data.real_trial_index = jsPsych.data.get().last(2).values()[0].real_trial_index;
    }
  };

  const repeat_prac_conditional = {
    timeline: [repeat_prac_message],
    conditional_function: function () {
      const trials_practice = jsPsych.data.get().filter({ block: 'practice', trial_name: 'array' }).last(blast_params.target_practice.length); // get last practice.length trials
      const n_correct_trials_practice = trials_practice.filter({ correct: 1 }).count(); // count only the true last 4 practice trials
      if (n_correct_trials_practice <= blast_params.target_practice.length / 2 && repeat_practice.length < blast_params.repeat_practice_times) {// if not more than half correct trials and we allow repeat_practice_times (normally 2) repetitions meaning practice is repeated.
        repeat_practice.push(true);
        return true;
      } else {
        repeat_practice.push(false);
        document.getElementsByClassName("jspsych-content-wrapper")[0].remove();
        return false;
      }
    }
  };

  practice.push({
    timeline: [waitmask5, waitmask1, block_practice, repeat_prac_conditional],
    loop_function: function (data) {
      if (repeat_practice[repeat_practice.length - 1] === true) { // if last practice is true, redo practice
        return true;
      } else {
        document.body.style.cursor = 'auto'; // display cursor during instructions
        return false;
      }
    },
  });

  await jsPsych.run(practice);

  // ************
  // *** MAIN ***
  // ************

  const arrays = blast_params.testonly == false ? // if testmode == true AND participant answered 0, testonly should be true and task would only show 10 trials
    generateArrays(blast_params.letter_array, blast_params.n_trials) :
    generateArrays(blast_params.letter_array, blast_params.n_test_trials);
  const target_main = arrays.target_main;
  const array_main = arrays.array_main;
  const present_main = arrays.trial_type;

  // Main Instructions
  main.push({
    type: jsPsychInstructions,
    data: {
      trial_name: "instructions",
      block: "main"
    },
    pages: [
      `Remember, you need to find the target letter within an array of 4 letters.<br>` +
      `If the ` + ` <span style="color:MediumSeaGreen;">target letter is present</span>` + `, press the ` + `<span style="color:MediumSeaGreen;"><b style="font-size:xx-large;">F</b>` + ` key with your left finger</span> ` +
      `If the ` + `<span style="color:Tomato;">target letter is NOT present</span>` + `, press the ` + `<span style="color:Tomato;"><b style="font-size:xx-large;">J</b>` + ` key with your right finger</span>`,
      `Be as FAST AND ACCURATE as you can.`
    ],
    show_clickable_nav: true,
    on_finish: function () {
      // Hide cursor
      document.body.style.cursor = "none";
    }
  })

  // Getting Ready: 5 sec mask5 followed by .8 sec mask1
  main.push({
    type: jsPsychHtmlKeyboardResponse,
    stimulus: getMask5(jsPsych, blast_params, 0),
    choices: `NO_KEYS`,
    trial_duration: blast_params.pres_time_mask5_intro,
    data: {
      trial_duration: blast_params.pres_time_mask5_intro,
      trial_name: "mask5_intro",
      real_trial_index: '',
      block: "main"
    },
    on_finish: function (data) {
      data.real_trial_index = 0;
    },
  });
  main.push({
    type: jsPsychHtmlKeyboardResponse,
    stimulus: getMask1(jsPsych, blast_params),
    choices: `NO_KEYS`,
    trial_duration: blast_params.pres_time_mask1_intro,
    data: {
      trial_duration: blast_params.pres_time_mask1_intro,
      trial_name: "mask1_intro",
      real_trial_index: '',
      block: "main"
    },
    on_finish: function (data) {
      data.real_trial_index = 0;
    },
  });

  // Timeline variables and timeline
  main.push({
    timeline_variables: createBlastTimelineVariables(
      target_main,
      array_main,
      present_main,
      jsPsych,
      blast_params
    ),
    timeline: createBlastTimeline("main", jsPsych, blast_params),
    on_timeline_finish: function (data) {
      // Show cursor
      document.body.style.cursor = "auto";
    }
  })

// Control questionnaires
  let survey_elements_end_p1 = [
    {
      type: 'boolean',
      title: 'Did you do the task in good conditions (e.g. without being disturbed)?',
      name: 'conditiongood',
      isRequired: true,
      labelTrue: "Yes",
      labelFalse: "No",
      valueTrue: "yes",
      valueFalse: "no",
    },
    {
      type: 'comment',
      title: 'Please give a short explanation.',
      name: 'conditioncomment',
      visibleIf: '{conditiongood} = "no"',
      isRequired: true,
    }
  ];
  let survey_elements_end_p2 = [
    {
      type: 'html',
      html: '<span class="sv-string-viewer sv-string-viewer--multiline">Please indicate your preferences in the use of hands in the following activities or objects. If you use both, select the one that you use the most.</span>',
    },
    {
      type: 'boolean',
      title: '✍️ Writing',
      name: 'writing',
      isRequired: true,
      labelTrue: "Right",
      labelFalse: "Left",
      valueTrue: "right",
      valueFalse: "left",
    },
    {
      type: 'boolean',
      title: '🤽 Throwing',
      name: 'throwing',
      isRequired: true,
      labelTrue: "Right",
      labelFalse: "Left",
      valueTrue: "right",
      valueFalse: "left",
    },
    {
      type: 'boolean',
      title: '🪥 Brushing your teeth',
      name: 'teeth',
      isRequired: true,
      labelTrue: "Right",
      labelFalse: "Left",
      valueTrue: "right",
      valueFalse: "left",
    },
    {
      type: 'boolean',
      title: '🥄 Using a spoon',
      name: 'spoon',
      isRequired: true,
      labelTrue: "Right",
      labelFalse: "Left",
      valueTrue: "right",
      valueFalse: "left",
    }
  ];

  main.push({
    type: jsPsychSurvey,
    data: {
      trial_name: "survey",
      block: "end-questionnaire"
    },
    survey_json: {
      showQuestionNumbers: false,
      pages: [
        {
          name: 'page1',
          elements: {
            type: 'html',
            html: '<span class="sv-string-viewer sv-string-viewer--multiline">The task is now over, we would like you to answer few questions before completing the session.<br>Please click on the "Next" button.</span>',
          },
        },
        {
          name: 'page2',
          elements: survey_elements_end_p1
        },
        {
          name: 'page3',
          elements: survey_elements_end_p2
        }
      ],
    },
    on_finish: function (data) {
      // add variables to data
      jsPsych.data.addProperties({
        condition_good: data.response.conditiongood,
        condition_comment: data.response.conditioncomment ? data.response.conditioncomment.replace(/\s/, ' ').replace(/[\W_]+/g," ") : "",
        writing: data.response.writing,
        throwing: data.response.throwing,
        teeth: data.response.teeth,
        spoon: data.response.spoon,
      });

      // Exit fullscreen
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) { /* Safari */
        document.webkitExitFullscreen();
      } else if (document.msExitFullscreen) { /* IE11 */
        document.msExitFullscreen();
      }

      data.response = '';
    }
  });

  // Save to db
  const ignoreCols = [ // This is the list of ignored columns from final data
    'success',
    'timeout',
    'failed_images',
    'failed_audio',
    'failed_video',
    'trial_type',
    'trial_index',
    'internal_node_id',
    'item_width_mm',
    'item_height_mm',
    'item_width_px',
    'px2mm',
    'view_dist_mm',
    'item_width_deg',
    'px2deg',
    'win_width_deg',
    'win_height_deg',
    'view_history',
    'stimulus',
    'plugin_version',
  ];
  if (blast_params.savedb) {
    main.push({
      type: jsPsychCallFunction,
      async: true,
      func: function (done) {
        // Log finish time
        jsPsych.data.addProperties({
          log_end: logCurrentDateTime(),
        });
        // Get and clean final data
        let final = jsPsych.data.get().ignore(ignoreCols);
        // Filter data to get only the relevant data
        final.trials = final.trials.filter(
          trial => trial.trial_name !== 'mask5_intro'
            && trial.trial_name !== 'mask1_intro'
            && trial.trial_name !== 'target'
            && trial.trial_name !== 'mask1'
            && trial.trial_name !== 'mask5'
        );
        // Setup POST HTTP request
        let xhr = new XMLHttpRequest();
        xhr.open('POST', blast_params.root_path + 'src/save.php');
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onload = function () {
          if (xhr.status == 200) {
            let response = xhr.responseText;
            console.log(response);
          }
          done(); // invoking done() causes experiment to progress to next trial.
        };
        xhr.send(final.json());
      }
    });
  }

  // End message
  main.push({
    type: jsPsychInstructions,
    data: {
      trial_name: "instructions",
      block: "main"
    },
    pages: [
      `Congratulations! You completed the task. We thank you for your participation.`
    ],
    show_clickable_nav: true,
    allow_backward: false,
    button_label_next: "Finish Experiment",
    on_finish: function () {
      // when finished and button not shown, redirect to tasks
      if (blast_params.redirect) {
        const p = document.createElement("p");
        p.setAttribute("style", "position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 16px; color: black;");
        p.innerText = blast_params.redirect_text;
        document.body.appendChild(p);
        setTimeout(() => {
          window.location.replace(blast_params.redirect_address);
          console.log(blast_params.redirect_text)
        }, 3000);
        return;
      }
      // Log finish time
      jsPsych.data.addProperties({
        log_end: logCurrentDateTime(),
      });
      // Get and clean final data
      let final = jsPsych.data.get().ignore(ignoreCols);
      // Filter data to get only the relevant data
      final.trials = final.trials.filter(
        trial => trial.trial_name !== 'mask5_intro'
          && trial.trial_name !== 'mask1_intro'
          && trial.trial_name !== 'target'
          && trial.trial_name !== 'mask1'
          && trial.trial_name !== 'mask5'
      );
      // Save data locally
      final.localSave('csv', 'blast_' + logCurrentDateTime().split(' ')[0].replace(/-/g, '') + '.csv'); // BACKEND : need to save this csv , otherwise uncomment for debugging

      // Backup button in case participant goes too fast
      const button = document.createElement("button");
      button.innerText = "Download CSV";
      button.id = "downloadCsvButton";
      button.style.cssText = "position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); padding: 10px 20px; font-size: 16px; background-color: #4CAF50; color: white; border: none; border-radius: 5px; cursor: pointer;";
      // Append the button to the body or any other element in the document
      document.body.appendChild(button);
      // Add the event listener to trigger final.localSave() on button click
      button.addEventListener("click", function () {
        if (typeof final !== "undefined" && typeof final.localSave === "function") {
          final.localSave('csv', 'blast_' + logCurrentDateTime().split(' ')[0].replace(/-/g, '') + '.csv'); // BACKEND : need to save this csv , otherwise uncomment for debugging
        } else {
          console.error("final.localSave() is not defined.");
        }
      });
    }
  })

  await jsPsych.run(main);
}

blast();