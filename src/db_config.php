<?php
  $env = file_get_contents("../.env");
  $lines = explode("\n",$env);

  foreach($lines as $line){
    preg_match("/([^#]+)\=(.*)/",$line,$matches);
    if(isset($matches[2])){ putenv(trim($line)); }
  } 

  $db_host = getenv("DB_HOST");
  $db_port = getenv("DB_PORT");
  $db_username = getenv("DB_USERNAME");
  $db_password = getenv("DB_PASSWORD");
  $db_name = getenv("DB_NAME");
  $db_table = getenv("DB_TABLE");
?>
