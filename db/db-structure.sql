/* db-structure.sql:
 * This is the structure of the database we used, which is compatible with the current code.
 * You may make modifications, but you will need to update the code used for the tasks accordingly.
 */

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tasks`
--

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

-- CREATE TABLE IF NOT EXISTS `subjects` (
--   `sid` int(10) unsigned NOT NULL AUTO_INCREMENT,
--   `username` varchar(50) DEFAULT NULL,
--   `creationTime` datetime DEFAULT NULL,
--   `locTime` datetime DEFAULT NULL,
--   PRIMARY KEY (`sid`)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blast`
--

CREATE TABLE IF NOT EXISTS `blast` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `trial_name` VARCHAR(30) DEFAULT NULL,
    `block` VARCHAR(30) DEFAULT NULL,
    `time_elapsed` DOUBLE DEFAULT NULL,
    `log_start` DATETIME DEFAULT NULL,
    `log_end` DATETIME DEFAULT NULL,
    `subject_id` VARCHAR(30) DEFAULT NULL,
    `study_id` VARCHAR(30) DEFAULT NULL,
    `session_id` VARCHAR(100) DEFAULT NULL,
    `monitor_size_inch` DOUBLE DEFAULT NULL,
    `_px2deg` DOUBLE DEFAULT NULL,
    `_view_dist_cm` DOUBLE DEFAULT NULL,
    `_letter_width_va` DOUBLE DEFAULT NULL,
    `_letter_width_cm` DOUBLE DEFAULT NULL,
    `_letter_dist_from_center_va` DOUBLE DEFAULT NULL,
    `_letter_dist_from_center_cm` DOUBLE DEFAULT NULL,
    `_monitor_width_px` INT(11) DEFAULT NULL,
    `_monitor_height_px` INT(11) DEFAULT NULL,
    `condition_good` VARCHAR(5) DEFAULT NULL,
    `condition_comment` VARCHAR(255) DEFAULT NULL,
    `writing` VARCHAR(10) DEFAULT NULL,
    `throwing` VARCHAR(10) DEFAULT NULL,
    `teeth` VARCHAR(10) DEFAULT NULL,
    `spoon` VARCHAR(10) DEFAULT NULL,
    `rt` INT(11) DEFAULT NULL,
    `browser` VARCHAR(30) DEFAULT NULL,
    `browser_version` VARCHAR(30) DEFAULT NULL,
    `mobile` VARCHAR(5) DEFAULT NULL,
    `os` VARCHAR(30) DEFAULT NULL,
    `fullscreen` VARCHAR(5) DEFAULT NULL,
    `vsync_rate` DOUBLE DEFAULT NULL,
    `response` VARCHAR(5) DEFAULT NULL,
    `trial_duration` INT(11) DEFAULT NULL,
    `real_trial_index` INT(11) DEFAULT NULL,
    `array_letters` VARCHAR(11) DEFAULT NULL,
    `target` VARCHAR(5) DEFAULT NULL,
    `present` TINYINT(1) DEFAULT NULL,
    `expected_response` VARCHAR(5) DEFAULT NULL,
    `correct` TINYINT(1) DEFAULT NULL,
    `trial_accuracy` VARCHAR(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;